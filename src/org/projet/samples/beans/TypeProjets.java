package org.projet.samples.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TYPEPROJETS")
public class TypeProjets {

	private List<TypeProjet> projets;

	@XmlElementWrapper(name = "typeProjets")
	@XmlElement(name = "typeProjet")
	public List<TypeProjet> getProjets() {
		return projets;
	}

	public void setProjets(List<TypeProjet> projets) {
		this.projets = new ArrayList<TypeProjet>(projets);
	}

}
