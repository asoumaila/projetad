package org.projet.samples.service;

import java.util.List;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.Competences;
import org.projet.samples.beans.TypeProjet;
import org.projet.samples.beans.TypeProjets;
import org.projet.samples.dao.IDao;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service implements IService {
	@Autowired
	private IDao dao;

	@Override
	public TypeProjet getTyprojet(int id) {
		// TODO Auto-generated method stub
		return dao.getOne(id);

	}

	public IDao getDao() {
		return dao;
	}

	public void setDao(IDao dao) {
		this.dao = dao;
	}

	@Override
	public void addprojet(TypeProjet projet) {
		// TODO Auto-generated method stub

		dao.addOneProjet(projet);

	}

	@Override
	public TypeProjet getProjetByName(String name) {
		// TODO Auto-generated method stub
		return dao.getOneByName(name);
	}

	@Override
	public Competence getComp(int id) {
		// TODO Auto-generated method stub
		return dao.getOneById(id);
	}

	@Override
	public Competence getCompByName(String name) {
		// TODO Auto-generated method stub
		return dao.getOneComp(name);
	}

	@Override
	public void addComp(Competence competence) {
		// TODO Auto-generated method stub
		dao.addOneComp(competence);

	}

	@Override
	public TypeProjets getAllProjet() {
		// TODO Auto-generated method stub
		return dao.getProjets();
	}

	@Override
	public Competences getAllComp() {
		// TODO Auto-generated method stub
		return dao.getComps();
	}

	@Override
	public List<Competence> getCompofprojet(int id_projet) {
		// TODO Auto-generated method stub
		return dao.getCompofty(id_projet);
	}

	@Override
	public List<Competence> getCompNotProjet(int id) {
		// TODO Auto-generated method stub
		return dao.getCompNotType(id);
	}

	@Override
	public void linkProjComp(int id_projet, int id_comp) {
		// TODO Auto-generated method stub

		dao.linkProjetComp(id_projet, id_comp);

	}

}
