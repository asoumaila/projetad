package org.projet.samples.dao;

import java.util.List;

import org.projet.samples.beans.Competence;
import org.projet.samples.beans.Competences;
import org.projet.samples.beans.TypeProjet;
import org.projet.samples.beans.TypeProjets;
import org.springframework.dao.DataAccessException;

public interface IDao {
	public TypeProjet getOne(int id);

	public void addOneProjet(TypeProjet projet);

	public TypeProjet getOneByName(String name) throws DataAccessException;

	public Competence getOneById(int id);

	public void addOneComp(Competence comp);

	public Competence getOneComp(String name) throws DataAccessException;

	public TypeProjets getProjets();

	public Competences getComps();

	public List<Competence> getCompofty(int id_projet);

	public List<Competence> getCompNotType(int id_projet);

	public void linkProjetComp(int id_projet, int id_comp);

}
