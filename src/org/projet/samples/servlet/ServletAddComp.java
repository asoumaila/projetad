package org.projet.samples.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Servlet implementation class ServletAddComp
 */
@WebServlet("/ServletAddComp")
public class ServletAddComp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IService service;

	public IService getService() {
		String chemin = "applicationContext-jdbc.xml";
		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		return service = (IService) context.getBean("service");
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletAddComp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		service = getService();
		String id = request.getParameter("id_projet");
		String id_com = request.getParameter("id_comp");
		int id_projet = Integer.parseInt(id);
		int id_comp = Integer.parseInt(id_com);
		System.out.print(id_projet);
		System.out.print(id_comp);

		// Add given comp to given project
		service.linkProjComp(id_projet, id_comp);

		// Prepare attribute of request for forward ...
		request.setAttribute("id_projet", id_projet);
		request.setAttribute("competences", service.getCompofprojet(id_projet));
		request.setAttribute("otherComp", service.getCompNotProjet(id_projet));
		// cette servlet envoie les donn�es � la jsp comp.jsp du coup je dois
		// avoir deux listes de competences celle li�e
		// au typedeprojet et son oppos�
		this.getServletContext().getRequestDispatcher("/Form/comp.jsp")
				.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
