package org.projet.samples.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Servlet implementation class ServletCompetence
 */
@WebServlet("/ServletCompetence")
public class ServletCompetence extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IService service;

	public IService getService() {
		String chemin = "applicationContext-jdbc.xml";
		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		return service = (IService) context.getBean("service");
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletCompetence() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String parm = request.getParameter("id_typeprojet");
		int id;
		if (!parm.equals("")) {
			id = Integer.parseInt(parm);
			service = getService();
			request.setAttribute("competences", service.getCompofprojet(id));
			request.setAttribute("otherComp", service.getCompNotProjet(id));
			request.setAttribute("id_projet", id);

			this.getServletContext().getRequestDispatcher("/Form/comp.jsp")
					.forward(request, response);

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
