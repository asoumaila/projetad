package org.projet.samples.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

import org.projet.samples.beans.TypeProjet;
import org.projet.samples.service.IService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServletTypeProjet extends HttpServlet {

	private IService service;

	public IService getService() {
		String chemin = "applicationContext-jdbc.xml";
		ApplicationContext context = new ClassPathXmlApplicationContext(chemin);
		return service = (IService) context.getBean("service");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void service(ServletRequest request, ServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		getService();

		List<TypeProjet> projets = service.getAllProjet().getProjets();

		request.setAttribute("projets", projets);

		this.getServletContext().getRequestDispatcher("/Form/projet.jsp")
				.forward(request, response);

	}
}
